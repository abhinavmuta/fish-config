function emacsd --description 'alias emacsd emacs --daemon'
	emacs --daemon $argv;
end
