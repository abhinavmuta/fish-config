# Path to Oh My Fish install.
set -q XDG_DATA_HOME
  and set -gx OMF_PATH "$XDG_DATA_HOME/omf"
  or set -gx OMF_PATH "$HOME/.local/share/omf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish

function fish_greeting
    # fortune -o
    fortune
end
funcsave fish_greeting

# eval (python -m virtualfish compat_aliases auto_activation)
